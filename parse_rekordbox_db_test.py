from os import path, system   # path to check if dest mp3 already exists; system to shell out to ffmpeg for actual conversion
from pyrekordbox.xml import RekordboxXml
import shlex # For escaping filepaths

debug = True
reconvert_if_already_done = False # later we can flip this to true to fix any files possibly broken in earlier bugged versions of this script

### ANSI color codes for colored terminal output
RED      = '\033[0;31m'
BOLD_RED = '\033[1;31m'

GREEN    = '\033[0;32m'
LPURP    = '\033[1;35m'
NC       = '\033[0m'   # No Color

DB_FILEPATH = "/Users/user/sandbox/rekordbox_db/rekordbox_db.xml"
NEW_DB_FILEPATH = "/Users/user/sandbox/rekordbox_db/rekordbox_db_modified.xml"

MAX_TRACKS_ALLOWED_TO_CONVERT = 10000000 # Used as a failsafe, set to a low number like 1 or 5 when debugging/testing changes
num_tracks_converted          = 0
num_skipped_files             = 0
num_failed_files              = 0

# Non-destructively convert track at old_path to 320kbps mp3 at new_path
# track_name just used for print statement
def convert(track_name, old_path, new_path):
    # Check that dest mp3 does not already exist
    if path.isfile(new_path) and not reconvert_if_already_done:
        print("[convert] {}Skipping{} {}{}{}, dest file already exists at {}".format(GREEN, NC, LPURP, track_name, NC, new_path))
        return "Skipped"

    old_path_escaped = shlex.quote(old_path)
    new_path_escaped = shlex.quote(new_path)

    FFMPEG_CONVERSION_CMD = "ffmpeg -i {} -vn -ab 320k -ar 44100 -y {}".format(old_path_escaped, new_path_escaped)
    print("Converting {} -> {} via running {}{}{}".format(track_name, new_path, LPURP, FFMPEG_CONVERSION_CMD, NC))

    ret_val = system(FFMPEG_CONVERSION_CMD)
    if debug:
        print("ret_val of convert {}:{}{}{}".format(track_name, LPURP, ret_val, NC))
    if ret_val != 0:
        print("{}Failed{} to convert {}: ret val of {}{}{}".format(BOLD_RED, NC, track_name, LPURP, ret_val, NC))
        return "Failed"
    # TODO: check return value more specifically and/or catch exceptions

xml = RekordboxXml(DB_FILEPATH)

for track in xml.get_tracks():
    track_id = track["TrackID"]
    name     = track["Name"]
    kind     = track["Kind"]
    location = track["Location"]

    if debug:
        print("Track ID {}, {}, is a {} track located at {}".format(track_id, name, kind, location))

    if kind == "FLAC File" and num_tracks_converted < MAX_TRACKS_ALLOWED_TO_CONVERT:
        print("Track {}{}{}, ID {}, is a {} track located at {}".format(LPURP, name, NC, track_id, kind, location))

        new_path = location[:-5] + '.mp3'
        # Run conversion command. For whatever reason location is missing the preceding forward-slash so manually add
        conversion_result = convert(name, '/' + location, '/' + new_path)
        if conversion_result == "Skipped":
            num_skipped_files += 1
        elif conversion_result == "Failed":
            num_failed_files += 1
        else:
            num_tracks_converted += 1

        track["Location"] = new_path
        track["Kind"]     = "MP3 File"
        # TODO: set track size to new correct value (otherwise it will still show the FLAC size which is annoying altho won't break anything)

print("Trying to save new db file to {}{}{}".format(LPURP, NEW_DB_FILEPATH, NC))
xml.save(NEW_DB_FILEPATH) # TODO: error handling

print("{}All done!{} Converted {}{}{} net-new tracks and skipped {} tracks due to existing mp3. Failed {}{}{} tracks".format(GREEN, NC, LPURP, num_tracks_converted, NC, num_skipped_files, BOLD_RED, num_failed_files, NC))
