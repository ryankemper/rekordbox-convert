### Dependencies

* Python (obviously)
* pyrekordbox (best installed via `pip3 install pyrekordbox`)
* ffmpeg

### Instructions

(0) Export current collection as XML (`File -> Export Collection in xml format`)

(1) Run script, feeding in appropriate filepaths for input xml file and output (modified) xml file

(2) Import the xml following the instructions here: https://www.manualslib.com/manual/1135837/Pioneer-Rekordbox.html?page=40

(3) Using the tree view, import the new playlists
